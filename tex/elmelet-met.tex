\section{A dolgozat elméleti háttere}

\subsection{Meteorológiai háttér}

Az Országos Meteorológiai Szolgálat 1870-ben alakult, akkor még Meteorológiai és Földdelejességi Magyar Királyi Központ néven, és már ekkor is 40 meteorológiai mérőállomás adatrögzítési tevékenységét foglalta össze, majd 1900-ra ez a szám 765-re nőtt \cite{omsz140eves}. Mivel ez a tevékenység – hogy bármilyen tudományos célra egyáltalán használható legyen – nagy precizitást és szervezettséget igényel, a mért adatok rögzítése mindig is egységes műszerekkel, módszerekkel, időpontokhoz kötve történt. Az adatrögzítéshez használt űrlapok és szoftverek fejlődésére mutat néhány példát az \ref{fig:urlapok}. ábra, \cite{metAdatbazisTortenete} alapján.

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/klima_1960_1996}
        \caption{1960--1996}
    \end{subfigure}
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/klima_1996_2001}
        \caption{1996--2001}
    \end{subfigure}
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/klima_2002_2003}
        \caption{2002--2003}
    \end{subfigure}
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/klima_2014_2015}
        \caption{2014--2015}
    \end{subfigure}
    }
    \caption{OMSZ meteorológiai észlelőfelület fejlődése, \cite{metAdatbazisTortenete} alapján}\label{fig:urlapok}
\end{figure}

Napjainkban már a társadalmi csapadékészlelők tevékenységétől eltekintve az OMSZ is számítógépes rendszereket használ az adatai nyilvántartására (pl. észleléshez gKTX/wKTX \cite{MAWOS}, a napi csapadék és a tízperces klímaautomaták méréseihez INDA \cite{OMSZHagyomanyosModszerek}).

\subsubsection{Meteorológiai mérések}

A meteorológiai méréseknek három alaptípusa van: közvetlen mérés, közvetett mérés (távérzékelés), és a vizuális észlelés. Az első két esetben műszeres mérés, a harmadiknál pedig egy emberi észlelő általi, szabad szemes megfigyelésről van szó \cite{ELTEMuszer}.

A meteorológiai állapotjelzők mérésének nagyon szigorú szabályai vannak \cite{WMOGuideObs}, amik nélkül ezek valójában alkalmatlanok lennének a használatra, mivel térben és időben folytonosan, nagymértékben változó, a mérés körülményeitől erősen függő paramétereket mérünk diszkrét mérési pontokban és időpontokban \cite{ELTEMuszer}. 

Például \cite{MorvaiK} vizsgálatai szerint két, egymás mellett, minden szempontból szabványos hőmérő által mért érték között, amik két eltérő árnyékolóban találhatóak, a körülményektől (pl. szél, globálsugárzás, talajfelszín állapota) függően akár 1 \textdegree{}C különbség is lehet.

Az adott mérésre vonatkozó követelmények valójában a felhasználás céljától függenek. Például szinoptikus célra olyan mérőhelyet kell választani, ami a környező kb. 100 km-es sugarú környezetet jól jellemzi \cite{ELTEMuszer}. Ez azt jelenti, hogy a mérőhely domborzata, felszínborítottsága megfelel a tágabb környezetnek, például a beépített területektől távolabb helyezkedik el, lehetőleg nem egy völgy alján \cite{foldfelsziniEloiras}. Egy tipikus mérőkertet mutat be az \ref{fig:muszerkert}. ábra \cite{MAWOS}. 

\begin{figure}[!htbp]
	\centering
    \shadowbox{\includegraphics[width=0.5\textwidth]{img/elmelet/muszerkert}}
    \caption{Egy repülőtéri műszerkert elvi felépítése \cite{MAWOS}}
    \label{fig:muszerkert}
\end{figure}

Ezzel szemben lokális skálájú jelenségek vizsgálatához néhány száz méter -- néhány kilométer felbontású, de speciálisabb elhelyezkedésű mérőműszerekre van szükség -- mint például a \cite{urbanTemperature} által bemutatott városi mérőhálózatok (\ref{fig:urbanmap}. ábra).

\begin{figure}[!htbp]
	\centering
    \shadowbox{\includegraphics[width=0.8\textwidth]{img/elmelet/urbanmap}}
    \caption{Példa városi mérőhálózatra (saját ábra, publikálva: \cite{urbanTemperature})}
    \label{fig:urbanmap}
\end{figure}

\subsubsection{Meteorológiai táviratok}

Mivel a meteorológia nagyon adatigényes tudományág, sokáig az adattovábbítás jelentette a szűk keresztmetszetet \cite{metInfo}. Emiatt lettek kifejlesztve nagyon tömör, de mégis ember számára jól olvasható távirat formátumok az adatok továbbításához \cite{omszSynop}. Különböző célokra más-más táviratokat használnak (pl. METAR, SPECI \cite{omszMetar}, SYNOP, TEMP, AIREP, DRIBU, SATEM\dots \cite{ELTEAlapismeretek}).
Például a jelenlegi legfrissebb METAR távirat \cite{ogimetMetar} Budapest-Ferihegyről így néz ki:

\begin{lstlisting}[language=bash,nolol=true]
METAR LHBP 212000Z 31005KT 8000 SCT004 BKN020 09/08 Q1014 NOSIG=
\end{lstlisting}

Ahol az egyes részek jelentése a következő, \cite{omszMetar} alapján:

\begin{itemize}
\item LHBP: Ferihegy repülőtér nemzetközi azonosítója
\item 21000Z: 21-én 20:00 UTC-kor készült
\item 31005KT: 310° szélirány, 5 csomós szélsebesség
\item 8000: 8 km-es látástávolság
\item SCT004: 3-4 okta felhőzet 400 láb (122 m) magasságban
\item BKN020: 5-7 okta felhőzet 2000 láb (610 m) magasságban
\item 09/08: hőmérséklet 9 °C, harmatpont 8 °C
\item Q1014: légnyomás 1014 hPa
\item NOSIG: nem várható változás
\end{itemize}

A legfrissebb SYNOP távirat \cite{ogimetSynop} Budapest-Pestszentlőrincről pedig ilyen:

\begin{lstlisting}[language=bash,nolol=true]
201610212000 AAXX 21201 12843 41362 83202 10085 20079 39978 40148 53007 70122 885// 333 82706 88626 555 10096=
\end{lstlisting}

Ahol az egyes részek jelentése a következő, \cite{foldfelsziniEloiras} alapján:

\begin{itemize}
\item 21201: 21-én 20:00 UTC, a szélsebesség mértékegysége m/s
\item 12843: Pestszentlőrinc SYNOP állomás WMO-azonosítója
\item 41362: csapadékadat nem áll rendelkezésre, észlelős állomás, jelenidőt észlelt, a legalacsonyabb felhőalap 2-300 m közötti, a látástávolság 12 km
\item 83202: 8 okta összfelhőzet, 320° szélirány, 2 m/s átlagos szélsebesség
\item 10085: hőmérséklet 8,5 °C
\item 20079: harmatpont 7,9 °C
\item 39978: állomásszinti légnyomás 997,8 hPa
\item 40148: tengerszinti légnyomás 1014,8 hPa
\item 53007: a légnyomás az elmúlt 3 órában csökkenő majd emelkedő, összességében 0,7 hPa-t emelkedett
\item 70122: a felhőzet az elmúlt órában vékonyodott, végig 5-8 okta volt
\item 885//: a felhőzet 8 okta Stratocumulus5 (nem cumulogenitus)
\item 82706 88626: még részletesebb adatok a felhőzetről
\item 10096: 96\% relatív nedvesség
\end{itemize}

\subsubsection{Mért meteorológiai állapotjelzők}

\subsubsection*{Hőmérséklet}

A hőmérséklet a testek termikus állapotát jellemző érték, a léghőmérséklet a légrészecskék mozgási energiájával arányos \cite{ELTEMuszer}. Térben és időben nagyon változékony, a gyakorlatban az ún. léghőmérséklet (T2) mérése a talajfelszín felett 2 méteres magasságban, rövidre nyírt fű fölött, hőmérőházban vagy árnyékoló alatt (\ref{fig:arnyekolok}. ábra) \cite{ELTEAlapismeretek}.

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/homerohaz1}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/homerohaz2}
    \end{subfigure}
    }
    \caption{Hagyományos Stevenson-féle hőmérőház és tányéros árnyékoló (saját fotók, publikálva: \cite{zivatarHu})}\label{fig:arnyekolok}
\end{figure}

Megkülönböztetünk továbbá fűszinti hőmérsékletet, amit szintén árnyékolóban mérünk, de a felszín felett 5 cm-rel, radiációs hőmérsékletet, amit 5 cm magasban, árnyékoló nélkül mérünk az éjszaka folyamán, valamint többféle mélységben elhelyezkedő talajhőmérőt.

Kutatási célra előfordulnak ettől eltérések is, például mikrometeorológiai mérések során gyakran ún. mérőtornyon találhatóak a műszerek, többféle magasságban \cite{ELTEMuszer}. Például a Hortobágyi Nemzeti Parkban 0,5 m és 6 m között 5 szinten \cite{ozone}, Hegyhátsálon pedig 10 m és 115 m között 4 szinten \cite{fluxMonitoring}.

Hagyományos analóg műszerekkel (az íróműszereket kivéve) csak pillanatnyi értéket, illetve két leolvasás közötti minimum (TN) és maximum (TX) értéket tudunk rögzíteni.

Létezik egyébként olyan hőmérő is, aminek nem szükséges az árnyékolás, mivel a szenzorának olyan kicsi a hőkapacitása, hogy gyakorlatilag nem képes az őt körülvevő léghőmérséklettől eltérő hőmérsékletet felvenni \cite{ELTEMuszer}, ilyen szenzorral vannak felszerelve a rádiószondák (\ref{fig:szondak}. ábra, jobb oldalon látható a fűszálaknál jóval vékonyabb platina szál, aminek az ellenállásváltozása révén a hőmérsékletet méri \cite{ELTEMuszer}).

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/szonda1}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/szonda2}
    \end{subfigure}
    }
    \caption{Vaisala rádiószonda, antenna és szenzor (saját fotók, publikálva: \cite{zivatarHu})}
    \label{fig:szondak}
\end{figure}

\subsubsection*{Nedvesség}

A légkörben a víz gyakorlatilag mindig jelen van, mennyisége térben és időben változatos, de a levegő összetételének kb. 0-4\%-át adja \cite{ELTEMuszer}. Számos nedvességi karakterisztikával jellemezhető, melyek közül az észlelési gyakorlatban a relatív nedvességet (RH) és a harmatpontot (TD) használjuk \cite{foldfelsziniEloiras}.

A relatív nedvesség alatt a levegőben ténylegesen jelen lévő vízgőz mennyiségének és az aktuális hőmérséklethez tartozó telítési vízgőz mennyiségnek a hányadosát értjük. Harmatpont alatt pedig azt a hőmérsékleti értéket, amire az aktuálisan jelen lévő levegőt állandó nyomás mellett lehűtve, telítetté válik \cite{ELTEMuszer}.

Tehát ha telítetlen levegőről beszélünk, akkor a harmatpont alacsonyabb a hőmérsékletnél, és a relatív nedvesség 100\% alatti. Telített levegő esetén a harmatpont és a hőmérséklet megegyezik, és a relatív nedvesség 100\%-os. Érdemes megemlíteni, hogy előfordul a természetben túltelített levegő is \cite{ELTEAlapismeretek}.

A nedvességmérés körülményeire vonatkozó követelmények megegyeznek a hőmérséklet mérésénél leírtakkal, ezért ezek általában közös árnyékolóban találhatóak \cite{foldfelsziniEloiras}.

\subsubsection*{Szélsebesség és szélirány}

A szél a levegő földfelszínhez viszonyított mozgásának vízszintes komponense. Vektormennyiség, azaz nem csak nagysága van (WS), hanem iránya (WD) is. Széliránynak a meteorológiában -- szemben például az oceanográfiával -- azt az irányt nevezzük, ahonnan a szél fúj \cite{ELTEMuszer}.

Míg a köznapi életben 8 főirányt és 8 mellékirányt különböztetünk meg (\ref{fig:szeliranyok}. ábra), a szinoptikus táviratba 10\textdegree{}-ra kerekítve adjuk meg a szél irányát \cite{foldfelsziniEloiras}.

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/szeliranyok}
    \end{subfigure}
    \caption{Szélirányok: főirányok, mellékirányok és fokok kapcsolata \cite{windDirections}}\label{fig:szeliranyok}
\end{figure}

A szél sebességét a felhasználási területtől függően m/s-ban, km/h-ban vagy csomóban szokták megadni. A szélsebesség vizuális észlelésére ad lehetőséget a klasszikus Beaufort-skála, ami a különböző tereptárgyak (füst, levelek, ágak, vízfelszín) viselkedése alapján ad egy becslést a szélsebességre \cite{omszSynop}.

Hagyományos szélmérő műszereket és a hangsebesség mérésén alapuló szonikus szélmérőt mutat be a \ref{fig:szelmerok}. ábra.

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/szelmerok1}
        \caption{Forgókanalas szélmérők}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/szelmerok2}
        \caption{Nyomólapos szélzászló}
    \end{subfigure}
    \begin{subfigure}[b]{0.3\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/szelmerok3}
        \caption{Szonikus szélmérő}
    \end{subfigure}
    }
    \caption{Különféle szélmérő műszerek (saját fotók, publikálva: \cite{zivatarHu})}
    \label{fig:szelmerok}
\end{figure}

\subsubsection*{Légnyomás}

Légnyomás alatt annak az erőnek a hatását értjük, amit egy vízszintes felületre a felette található légoszlop fejt ki, a saját súlyából adódóan. SI egysége ugyan a Pascal, de a meteorológiában a hPa az elfogadott \cite{ELTEMuszer}. Mivel nagyon erősen hőmérsékletfüggő, a légnyomást mérő barométert állandó szobahőmérsékleten kell tartani \cite{foldfelsziniEloiras}.

Mivel a légnyomás időjárásból eredő változékonysága lényegesen kisebb, mint a magasságkülönbségből adódó légnyomáskülönbség, így a gyakorlati felhasználáshoz a mért értékeket egy közös magassági szintre kell korrigálni. A nem hegyvidéki állomások számára ez a közös szint a tengerszint. Tehát a tengerszintre átszámított légnyomást (RP) úgy kapjuk, hogy feltételezzük, hogy az alattunk lévő térrészt a tengerszintig levegő töltené ki, az ennek a súlyából adódó légnyomást adjuk hozzá a valós, műszerszinti (AP) légnyomáshoz \cite{ELTEMuszer}.

Az egy időponthoz tartozó tengerszinti légnyomást (izobárok) és annak változását (izallobárok) ábrázoló szinoptikus térkép (\ref{fig:szinopterkep}. ábra) -- vagy más néven talajtérkép -- nagyon jól kifejezi az aktuális nagytérségű időjárási helyzet jellegét, így az előrejelzők számára fontos szerepe van \cite{OMSZHagyomanyosModszerek}.

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/synopterkep}
    \end{subfigure}
    }
    \caption{Szinoptikus térkép \cite{metOfficeSynopChart}}
    \label{fig:szinopterkep}
\end{figure}

\subsubsection*{Csapadékmennyiség}

Csapadékmennyiség alatt azt a vízréteg vastagságot értjük, ami a felszínt borítaná a csapadékhullás végén, ha közben a víz nem folyna el, nem szivárogna be a talajba, és nem is párologna el. Mértékegysége mm, ami megfelel a l/m$^{2}$-nek \cite{ELTEMuszer}.

Többféle elven működő csapadékmérőt használunk, ezek egy része gyűjti a csapadékvizet, amit manuális észlelés során lemérhetünk, másik része pedig egy billenőedényen engedi át, és a billenések számát regisztrálja (\ref{fig:csapimero}. ábra). Ezen kívül léteznek pl. lézeres csapadékmérők is \cite{ELTEMuszer}.

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/csapi1}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/csapi2}
    \end{subfigure}
    }
    \caption{Hagyományos és automata csapadékmérő műszer (saját fotók, publikálva: \cite{zivatarHu})}
    \label{fig:csapimero}
\end{figure}

\subsubsection*{Hóvastagság}

A talajt fedő hóréteg vastagságát egész centiméterre kerekítve kell megadni, minimum három mérés átlagát képezve. A mérések helyét is úgy kell kiválasztani, hogy egy átlagos felszínt reprezentáljon. Ha nem összefüggő a hóréteg, akkor „hófoltok” vannak, ha pedig összefüggő, de a vastagsága nem éri el az 1 cm-t, akkor „hólepel” \cite{foldfelsziniEloiras}.

\subsubsection*{Felhőzet mennyisége}

Felhőzet mennyiségén a teljes égbolt felhőkkel borított részének arányát értjük. Oktában, azaz nyolcadban adjuk meg. A két szélsőértéknél nem kerekítünk, csak akkor adunk 0 oktát, ha teljesen felhőmentes az ég, és csak akkor 8 oktát, ha az ég minden része felhővel borított. Ezt az észlelő szemrevételezés során becsüli meg \cite{foldfelsziniEloiras}.

\subsubsection*{Felhőzet típusa}

A felhőket a magasságuk alapján három csoportba osztjuk \cite{ELTEAlapismeretek}. Az egyes felhőfajták azonosítása az észlelő által történik \cite{foldfelsziniEloiras}. A pontos azonosítás a 10 alaptípusba, ezek fajtáiba, változataiba, továbbá kísérőfelhőik és képződésük alapján felhőatlaszok pl. \cite{WMOClouds1}, \cite{WMOClouds2}, \cite{onlineCloudAtlas} segítségével történik (\ref{fig:felhofajtak}. ábra).

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/felhofajtak}
    \end{subfigure}
    }
    \caption{Felhőfajták összefoglaló táblázata \cite{cloudChart}}
    \label{fig:felhofajtak}
\end{figure}

A szinoptikus gyakorlatban ennek egy leegyszerűsített változatát használjuk \cite{cloudTypes} amely során az égbolt egészét, annak jellegét és változásait jellemezzük magassági szintenként 1-1 számmal (\ref{tab:cloudTypes}. táblázat).

\begin{table}[!htbp]
  \caption {A szinoptikus égkép-osztályozás alaptípusai \cite{foldfelsziniEloiras} alapján} 
  \centering
  \begin{tabularx}{\textwidth}{l X X X}
  & Alacsony szintű égkép & Középmagas égkép & Magas szintű égkép \\
  \hline
  0 & -- & -- & -- \\
  1 & Cu hum vagy Cu fra & As tra & Ci fib \\
  2 & Cu med vagy Cu con & As opa vagy Ns & Ci spi \\
  3 & Cb cal & Ac tra & Ci spi cbgen \\
  4 & Sc cugen vagy Sc sbgen & Ac len & felvonuló Ci \\
  5 & Sc & felvonuló Ac & felvonuló Cs 45° alatt \\
  6 & St & Ac cugen vagy Ac cbgen & felvonuló Cs 45°fölött \\
  7 & St fra vagy Cu fra & Ac vagy As & 8 okta Cs \\
  8 & Cu és Sc különböző szinteken & Ac flo vagy Ac cas & nem felvonuló Cs \\
  9 & Cb cap & kaotikus égkép & Cc \\
  \hline
\end{tabularx}
\label{tab:cloudTypes}
\end{table}

\subsubsection*{Szignifikáns időjárási jelenségek}

A szignifikáns időjárási jelenségek alatt azokat az eseményeket értjük, melyek lehetnek vízjelenségek, porjelenségek, fényjelenségek, vagy elektromos jelenségek \cite{ELTEMuszer}. A szinoptikus meteorológiai gyakorlatban a jelenlegi időjárás jellemzésére 99 + 1 kategóriát különböztetünk meg, amik közül sok egymáshoz közel álló (pl. nyílt/zárt köd ami az elmúlt órában gyengült/erősödött/nem változott).  A legutóbbi észlelés óta eltelt időtartam jellemzésére pedig 10 kategória adott \cite{foldfelsziniEloiras}.

Az észlelendő kategóriák optimális száma szubjektív, egyrészt nem várható el egy átlagos felhasználótól az, hogy egy 100 elemű listában bogarásszon, másrészt a 10 elemű felosztás mindenképp egyszerűsítést, összevonást tartalmaz. Az adatok tervezett felhasználása szempontjától is függ, hogy melyik információ fontos. Az előrejelzőknek szóló szinoptikus táviratba a magasabb kódszámú jelenség fog bekerülni \cite{foldfelsziniEloiras} (80-tól záporos csapadékok, 90-től zivatar), pedig egy erős ónos eső (67), erős magas hófúvás (39) vagy tornádó (19) előfordulása valószínűleg jobban érdekli az átlagembert.

\subsubsection*{Légköroptikai jelenségek}

Gyakorlati jelentőségük híján a légköroptikai jelenségek megfigyelése a hivatalos észlelési gyakorlatban csupán a szivárványra korlátozódik \cite{foldfelsziniEloiras}, pedig az érdekességük és szépségük (\ref{fig:legkoroptika}. ábra) miatt sokan megfigyelik őket \cite{haloKosaKiss}, bár sajnos az átlagemberek legtöbbször hibásan értelmezik ezeket a jelenségeket.

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/legkoroptika1}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/legkoroptika2}
    \end{subfigure}
    }
    \caption{Légköroptikai jelenségek (saját fotók, publikálva: \cite{zivatarHu})}
    \label{fig:legkoroptika}
\end{figure}

Például \cite{foldrengesFeny} földrengésfényként mutat be egy zenit körüli ívet \cite{haloLegkor}, \cite{HAARP} pedig ugyanezt a képet -- kisebb utólagos módosítással -- mint a HAARP-féle összeesküvés-elmélet bizonyítékát. Emiatt tudománynépszerűsítő szempontból van jelentősége, hogy a légköroptikai jelenségekkel is foglalkozzunk.

A légköroptikai jelenségeknek csupán az előfordulását fogjuk rögzíteni, fajtánként, amiket egy listából lehet kiválasztani, elöl a gyakoribb típusokkal (22\textdegree{}-os halo, melléknap, felső érintő ív, stb. \cite{haloLegkor}).

\subsubsection{Éghajlati jellemszámok}

A meteorológiai és klimatológiai gyakorlatban, mivel nagyon sok állapotjelző térbeli és időbeli alakulását kell nyomon követni, általánosan elterjedt a jellemszámok és indexek használata, mivel ezek segítségével egyszerűsíthető az átlátni kívánt adatmennyiség (pl. \cite{WMOGuideExtreme}, \cite{convectiveIndices}). Mivel szinte végtelen számú különböző index létezik, ezek közül csak néhány népszerűbbet fogunk kiszámítani és megjeleníteni. Néhány definíciót az alábbiakban ismertetek (\ref{fig:indices}.ábra).

\begin{figure}[!htbp]
    \centering
    \begin{subfigure}[b]{0.95\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/indices}
    \end{subfigure}
    \caption{Leggyakoribb éghajlati jellemszámok definíciója \cite{ELTEAlapismeretek}}
    \label{fig:indices}
\end{figure}


\subsubsection{Amatőr meteorológus mérőműszerek}

Manapság bárki számára elérhetők jó minőségű meteorológiai mérőműszerek (pl. \cite{Conrad}, \cite{muszerTeszt}, \cite{ws3600}). Ezek általában rendelkeznek egy központi – adatgyűjtő – egységgel, és néhány szenzorral, általában tartozik hozzájuk egy hőmérséklet + relatív nedvesség szenzor, egy szélirány + szélsebesség szenzor és egy csapadékmérő.

Az adatgyűjtő kijelzi az aktuális értékeket, rosszabb esetben lekérhetőek egy napra vonatkozó összesített értékek, a jobbak viszont számítógépes kapcsolattal rendelkeznek, és 10 percenként mért adatokat továbbítanak a PC-re (pl. LaCrosse WS-3600 \cite{ws3600} vagy TechnoLine WS-2800 (\ref{fig:ws-2800}. ábra)).

\begin{figure}[!htbp]
    \centering
    \shadowbox{
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/ws2800-1}
    \end{subfigure}
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/ws2800-2}
    \end{subfigure}
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/ws2800-3}
    \end{subfigure}
    \begin{subfigure}[b]{0.23\textwidth}
        \includegraphics[width=\textwidth]{img/elmelet/ws2800-4}
    \end{subfigure}
    }
    \caption{TechnoLine WS-2800 főbb részegységei (saját felvételek)}
    \label{fig:ws-2800}
\end{figure}

Az adatokat a műszerhez tartozó kezelőszoftver kéri le és tárolja (pl. Heavy Weather \cite{heavyWeather}, Weather Display \cite{weatherDisplay}). Ki is tudja exportálni plain text formátumba.