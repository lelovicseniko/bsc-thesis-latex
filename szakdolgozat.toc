\select@language {magyar} \contentsline {section}{\numberline {1}Bevezet\IeC {\'e}s}{4}{section.1}
\select@language {magyar} \contentsline {subsection}{\numberline {1.1}C\IeC {\'e}lkit\IeC {\H u}z\IeC {\'e}s}{4}{subsection.1.1}
\select@language {magyar} \contentsline {section}{\numberline {2}A dolgozat elm\IeC {\'e}leti h\IeC {\'a}ttere}{5}{section.2}
\select@language {magyar} \contentsline {subsection}{\numberline {2.1}Meteorol\IeC {\'o}giai h\IeC {\'a}tt\IeC {\'e}r}{5}{subsection.2.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.1.1}Meteorol\IeC {\'o}giai m\IeC {\'e}r\IeC {\'e}sek}{5}{subsubsection.2.1.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.1.2}Meteorol\IeC {\'o}giai t\IeC {\'a}viratok}{7}{subsubsection.2.1.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.1.3}M\IeC {\'e}rt meteorol\IeC {\'o}giai \IeC {\'a}llapotjelz\IeC {\H o}k}{8}{subsubsection.2.1.3}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.1.4}\IeC {\'E}ghajlati jellemsz\IeC {\'a}mok}{15}{subsubsection.2.1.4}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.1.5}Amat\IeC {\H o}r meteorol\IeC {\'o}gus m\IeC {\'e}r\IeC {\H o}m\IeC {\H u}szerek}{15}{subsubsection.2.1.5}
\select@language {magyar} \contentsline {subsection}{\numberline {2.2}Szoftverk\IeC {\"o}rnyezet \IeC {\'e}s felhaszn\IeC {\'a}lt technol\IeC {\'o}gi\IeC {\'a}k}{17}{subsection.2.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.2.1}Szerveroldali programoz\IeC {\'a}s MVC keretrendszerrel}{17}{subsubsection.2.2.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.2.2}Webes keretrendszerek Python-hoz}{18}{subsubsection.2.2.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.2.3}A Django}{19}{subsubsection.2.2.3}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.2.4}Felhaszn\IeC {\'a}lt szerveroldali f\IeC {\"u}ggv\IeC {\'e}nyk\IeC {\"o}nyvt\IeC {\'a}rak}{26}{subsubsection.2.2.4}
\select@language {magyar} \contentsline {subsubsection}{\numberline {2.2.5}Kliensoldali programoz\IeC {\'a}s, felhaszn\IeC {\'a}lt f\IeC {\"u}ggv\IeC {\'e}nyk\IeC {\"o}nyvt\IeC {\'a}rak}{28}{subsubsection.2.2.5}
\select@language {magyar} \contentsline {section}{\numberline {3}Tervez\IeC {\'e}s \IeC {\'e}s el\IeC {\H o}k\IeC {\'e}sz\IeC {\'\i }t\IeC {\'e}s}{33}{section.3}
\select@language {magyar} \contentsline {subsection}{\numberline {3.1}Funkcion\IeC {\'a}lis specifik\IeC {\'a}ci\IeC {\'o}, a felhaszn\IeC {\'a}l\IeC {\'o}i fel\IeC {\"u}let tervez\IeC {\'e}se}{33}{subsection.3.1}
\select@language {magyar} \contentsline {subsection}{\numberline {3.2}Az adatmodell, az adatb\IeC {\'a}zis}{38}{subsection.3.2}
\select@language {magyar} \contentsline {section}{\numberline {4}Megval\IeC {\'o}s\IeC {\'\i }t\IeC {\'a}s}{44}{section.4}
\select@language {magyar} \contentsline {subsection}{\numberline {4.1}A fejleszt\IeC {\H o}i k\IeC {\"o}rnyezet}{44}{subsection.4.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.1.1}Django telep\IeC {\'\i }t\IeC {\'e}se a fejleszt\IeC {\H o}i sz\IeC {\'a}m\IeC {\'\i }t\IeC {\'o}g\IeC {\'e}pre}{44}{subsubsection.4.1.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.1.2}Django projekt \IeC {\'e}s applik\IeC {\'a}ci\IeC {\'o} l\IeC {\'e}trehoz\IeC {\'a}sa}{45}{subsubsection.4.1.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.1.3}Dokument\IeC {\'a}ci\IeC {\'o}}{45}{subsubsection.4.1.3}
\select@language {magyar} \contentsline {subsection}{\numberline {4.2}A fejleszt\IeC {\'e}s}{46}{subsection.4.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.2.1}Szerver oldal}{46}{subsubsection.4.2.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {4.2.2}Kliens oldal}{47}{subsubsection.4.2.2}
\select@language {magyar} \contentsline {subsection}{\numberline {4.3}Nagyobb probl\IeC {\'e}m\IeC {\'a}k a fejleszt\IeC {\'e}s sor\IeC {\'a}n}{51}{subsection.4.3}
\select@language {magyar} \contentsline {subsection}{\numberline {4.4}Telep\IeC {\'\i }t\IeC {\'e}s szerverre}{53}{subsection.4.4}
\select@language {magyar} \contentsline {section}{\numberline {5}Tesztel\IeC {\'e}s}{55}{section.5}
\select@language {magyar} \contentsline {subsection}{\numberline {5.1}Hozz\IeC {\'a}f\IeC {\'e}r\IeC {\'e}si adatok}{55}{subsection.5.1}
\select@language {magyar} \contentsline {subsection}{\numberline {5.2}Alapvet\IeC {\H o} m\IeC {\H u}k\IeC {\"o}d\IeC {\'e}s}{55}{subsection.5.2}
\select@language {magyar} \contentsline {subsection}{\numberline {5.3}Autentik\IeC {\'a}ci\IeC {\'o}}{56}{subsection.5.3}
\select@language {magyar} \contentsline {subsection}{\numberline {5.4}Regisztr\IeC {\'a}ci\IeC {\'o}}{56}{subsection.5.4}
\select@language {magyar} \contentsline {subsubsection}{\numberline {5.4.1}Bel\IeC {\'e}p\IeC {\'e}s, kil\IeC {\'e}p\IeC {\'e}s}{57}{subsubsection.5.4.1}
\select@language {magyar} \contentsline {subsection}{\numberline {5.5}Publikus/saj\IeC {\'a}t m\IeC {\'e}r\IeC {\H o}helyek list\IeC {\'a}ja}{58}{subsection.5.5}
\select@language {magyar} \contentsline {subsection}{\numberline {5.6}Publikus/saj\IeC {\'a}t m\IeC {\'e}r\IeC {\H o}hely adatlapja}{58}{subsection.5.6}
\select@language {magyar} \contentsline {subsection}{\numberline {5.7}\IeC {\'E}ghajlati adatok megjelen\IeC {\'\i }t\IeC {\'e}se}{59}{subsection.5.7}
\select@language {magyar} \contentsline {subsection}{\numberline {5.8}\IeC {\'E}ghajlati napl\IeC {\'o} kit\IeC {\"o}lt\IeC {\'e}se}{59}{subsection.5.8}
\select@language {magyar} \contentsline {subsection}{\numberline {5.9}M\IeC {\'e}rt adatok felt\IeC {\"o}lt\IeC {\'e}se}{60}{subsection.5.9}
\select@language {magyar} \contentsline {subsection}{\numberline {5.10}Aktu\IeC {\'a}lis id\IeC {\H o}j\IeC {\'a}r\IeC {\'a}s \IeC {\'e}szlel\IeC {\'e}se}{60}{subsection.5.10}
\select@language {magyar} \contentsline {subsection}{\numberline {5.11}\IeC {\'U}j m\IeC {\'e}r\IeC {\H o}hely l\IeC {\'e}trehoz\IeC {\'a}sa}{60}{subsection.5.11}
\select@language {magyar} \contentsline {subsection}{\numberline {5.12}L\IeC {\'e}tez\IeC {\H o} m\IeC {\'e}r\IeC {\H o}hely szerkeszt\IeC {\'e}se}{61}{subsection.5.12}
\select@language {magyar} \contentsline {subsection}{\numberline {5.13}\IeC {\'U}j m\IeC {\'e}r\IeC {\H o}m\IeC {\H u}szer hozz\IeC {\'a}ad\IeC {\'a}sa}{61}{subsection.5.13}
\select@language {magyar} \contentsline {subsection}{\numberline {5.14}Admin fel\IeC {\"u}let}{62}{subsection.5.14}
\select@language {magyar} \contentsline {section}{\numberline {6}\IeC {\"U}zemeltet\IeC {\H o}i le\IeC {\'\i }r\IeC {\'a}s}{63}{section.6}
\select@language {magyar} \contentsline {subsection}{\numberline {6.1}A webapp telep\IeC {\'\i }t\IeC {\'e}shez sz\IeC {\"u}ks\IeC {\'e}ges k\IeC {\"o}rnyezet}{63}{subsection.6.1}
\select@language {magyar} \contentsline {subsection}{\numberline {6.2}A Django \IeC {\'e}s dependenci\IeC {\'a}inak telep\IeC {\'\i }t\IeC {\'e}se}{63}{subsection.6.2}
\select@language {magyar} \contentsline {subsection}{\numberline {6.3}A Django konfigur\IeC {\'a}l\IeC {\'a}sa}{64}{subsection.6.3}
\select@language {magyar} \contentsline {subsection}{\numberline {6.4}Apache konfigur\IeC {\'a}l\IeC {\'a}sa}{64}{subsection.6.4}
\select@language {magyar} \contentsline {subsection}{\numberline {6.5}Verzi\IeC {\'o}friss\IeC {\'\i }t\IeC {\'e}sek}{65}{subsection.6.5}
\select@language {magyar} \contentsline {section}{\numberline {7}Felhaszn\IeC {\'a}l\IeC {\'o}i le\IeC {\'\i }r\IeC {\'a}s}{66}{section.7}
\select@language {magyar} \contentsline {subsection}{\numberline {7.1}Rendszerk\IeC {\"o}vetelm\IeC {\'e}nyek}{66}{subsection.7.1}
\select@language {magyar} \contentsline {subsection}{\numberline {7.2}Publikus fel\IeC {\"u}let}{66}{subsection.7.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.1}\IeC {\'U}j felhaszn\IeC {\'a}l\IeC {\'o} regisztr\IeC {\'a}ci\IeC {\'o}ja}{66}{subsubsection.7.2.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.2}Jelsz\IeC {\'o}v\IeC {\'a}ltoztat\IeC {\'a}s}{68}{subsubsection.7.2.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.3}Felhaszn\IeC {\'a}l\IeC {\'o} adatai}{68}{subsubsection.7.2.3}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.4}M\IeC {\'e}r\IeC {\H o}helyek list\IeC {\'a}ja}{70}{subsubsection.7.2.4}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.5}M\IeC {\'e}r\IeC {\H o}helyek adatlapja}{71}{subsubsection.7.2.5}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.6}\IeC {\'E}szlel\IeC {\'e}sek}{71}{subsubsection.7.2.6}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.7}M\IeC {\'e}r\IeC {\H o}m\IeC {\H u}szerek}{72}{subsubsection.7.2.7}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.8}\IeC {\'E}ghajlati adatok}{73}{subsubsection.7.2.8}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.2.9}\IeC {\'E}szlel\IeC {\'e}si seg\IeC {\'e}dlet}{73}{subsubsection.7.2.9}
\select@language {magyar} \contentsline {subsection}{\numberline {7.3}\IeC {\'E}szlel\IeC {\H o}i fel\IeC {\"u}let}{73}{subsection.7.3}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.3.1}M\IeC {\'e}r\IeC {\H o}helyek l\IeC {\'e}trehoz\IeC {\'a}sa, karbantart\IeC {\'a}sa}{73}{subsubsection.7.3.1}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.3.2}M\IeC {\'e}r\IeC {\H o}m\IeC {\H u}szerek karbantart\IeC {\'a}sa}{73}{subsubsection.7.3.2}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.3.3}\IeC {\'E}ghajlati napl\IeC {\'o} kit\IeC {\"o}lt\IeC {\'e}se}{77}{subsubsection.7.3.3}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.3.4}Automata m\IeC {\H u}szer adatainak import\IeC {\'a}l\IeC {\'a}sa}{77}{subsubsection.7.3.4}
\select@language {magyar} \contentsline {subsubsection}{\numberline {7.3.5}Vizu\IeC {\'a}lis \IeC {\'e}szlel\IeC {\'e}s}{78}{subsubsection.7.3.5}
\select@language {magyar} \contentsline {subsection}{\numberline {7.4}Adminisztr\IeC {\'a}tori fel\IeC {\"u}let}{79}{subsection.7.4}
\select@language {magyar} \contentsline {section}{\numberline {8}\IeC {\"O}sszefoglal\IeC {\'a}s}{81}{section.8}
\select@language {magyar} \contentsline {subsection}{\numberline {8.1}Tov\IeC {\'a}bbfejleszt\IeC {\'e}si lehet\IeC {\H o}s\IeC {\'e}gek}{81}{subsection.8.1}
\select@language {magyar} \contentsline {section}{\'Abr\'ak jegyz\'eke}{83}{section*.93}
\select@language {magyar} \contentsline {section}{T\'abl\'azatok jegyz\'eke}{84}{section*.94}
\select@language {magyar} \contentsline {section}{\numberline {9}Hivatkoz\'asok}{85}{section.9}
